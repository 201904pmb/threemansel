<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
  <style>
  h1{
  text-align: center;
  }
  .center{
  text-align: center;
  }
  </style>

  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="../css/style2.css">
  <link rel="stylesheet" type="text/css" href="../css/style3.css">
  <link rel="stylesheet" type="text/css" href="../css/style4.css">
  <link rel="stylesheet" type="text/css" href="../css/fontSize.css">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>作成者紹介</title>
</head>

<body>
<h1>自己紹介</h1>
<h3>名前</h3>
・清原 尚也(キヨハラ ナオヤ)<p>
<h3>性別</h3>
男
<h3>生年月日</h3>
1995年3月16日
<h3>趣味</h3>
・AI開発
・ゲーム
<h3>自己PR</h3>
<div class="box6">
  AIとPythonを独学で3年間勉強してきました。
</div>

<h1>作品一覧</h1>
<h3>C#(Windowsアプリケーション)</h3>

<h3>数字認識アプリ</h3>
<p>
<div class="box2">
<a href="\threeManSel\openFolder">
<img src = "../image/kiyohara/mnist.png">
</a>
</div>

<div class="box6">
深層学習 + ペイントルーツを組み合わせたアプリケーション
Pythonで学習をさせて、C#で読み込むという事をしている。
ペイントツールはc#
</div>

<h3>原子モンテカルロ計算機</h3>
<p>
<div class="box6">
<a href="\threeManSel\PlayServlet?param=work%5Ckiyohara%5Cc%23%5Cmontecarlo%5CmonteCarlo%5CmonteCarlo%5Cbin%5CDebug%5CmonteCarlo.exe">
<img src="../image/kiyohara/mc.png">
</a>
</div>

<div class="box6">
原子モンテカルロを用いた確率算出アプリケーション
</div>

<h3>モンテカルロUCTの視覚化</h3>
<p>
<div class="box2">
<a href="\threeManSel\PlayServlet?param=work%5Ckiyohara%5Cc%23%5CmontecarloUCT%5CmonteCarloUct%5Cbin%5CDebug%5CmonteCarloUct.exe">
  <img src="../image/kiyohara/mcuct.png">
</a>
</div>

<div class="box6">
モンテカルロUCTを視覚的にわかりやすくした作品
</div>

<a href="home.jsp">戻る</a>
</body>
</html>