<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
h1{
text-align: center;
}
.center{
text-align: center;
}
</style>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/style2.css">
<link rel="stylesheet" type="text/css" href="../css/style3.css">
<link rel="stylesheet" type="text/css" href="../css/style4.css">
<link rel="stylesheet" type="text/css" href="../css/fontSize.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>自己紹介</h1>
<h3>名前</h3>
・望月　康治(モチヅキ　ヤスジ)<p>
<h3>性別</h3>
・男
<h3>生年月日</h3>
・1967年7月22日
<h3>趣味</h3>
・DVD・CD鑑賞
<h3>自己PR</h3>
<div class="box6">
    <p>
        残り少ない人生をプログラマーとして過ごしたいと思い、
        この学校に入校しました。
        </p>
        <p>
        プログラミングは難しくて、なかなか自分の納得がいく作品ができませんが、
        それでも、何かしらの作品が完成したときは、
        ほんの少しでも達成感が得られて、少しうれしくなります。
        </p>
        <p>
        学校でいろんな事を学びましたが、結構たくさん忘れているので、
        これで仕事ができるのか不安になりますが、
        なんとかプログラマーの職に就けるように、
        がんばって就職活動をしたいと思います。
        </p>
</div>
<h1>自分の作品</h1>

<h3>Unity</h3>
<h3>ブロック崩し</h3>
<p>
<div class="box2">
<!--
<a href="../Breakout/index.html">

<a href="\threeManSel\PlayServlet?param=C%3A%5CUsers%5C190416PM%5CDocuments%5CUnity%5CBuild%5CMochizuki%5CBreakout%5CBreakout.exe">
    <img src="../picture/Breakout.png">
</a>
-->

<a href="http://localhost:8080/threeManSel/PlayServlet?param=work%5Cmotiduki%5Cunity%5CBreakout%5CBreakout.exe">
    <img src="../picture/Breakout.png">
</a>

</div>

<div class="box6">
・見た目はともかく、とりあえずゲームの体裁だけは、
整えました。ただ、見た目やゲームデザインなどゲームを面白くするには、
工夫、改善する余地は、たくさんあると思います。
</div>

<h3>C#</h3>
<h3>フラッシュ暗算</h3>
<p>
<div class="box2">
<a href="http://localhost:8080/threeManSel/PlayServlet?param=work%5Cmotiduki%5Cc%23%5CFlashANZAN%5CFlashANZAN%5Cbin%5CDebug%5CFlashANZAN.exe">
    <img src="../picture/FlashANZAN.png">
</a>
</div>

<div class="box6">
・制作期間内で数字をフラッシュさせることはできませんでしたが、
数字をフラッシュさせて、繰り返しプレーできるようにすれば、
ゲームとして成立すると思います。
</div>

<a href="home.jsp">戻る</a>
</body>
</html>
