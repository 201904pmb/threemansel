<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
h1{
text-align: center;
}
.center{
text-align: center;
}
h3{
text-align: center;
}
.center{
text-align: center;
}

div{
text-align: center;
}
.center{
text-align: center;
}
</style>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/style2.css">
<link rel="stylesheet" type="text/css" href="../css/style3.css">
<link rel="stylesheet" type="text/css" href="../css/style4.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>職業訓練校2019年度4月生チーム開発</title>
</head>
<body>
      <h1>職業訓練校について</h1>
      <h3>職業訓練校とは</h3>
<div class="box6">職業訓練校とは、失業中の人が再就職するための公共職業訓練を行う施設です。国や自治体が主体となって運営しており、年間30万人に利用されています。私たちは、
      この職業訓練校で半年間「Wed・ゲーム・アプリ」の開発知識の基礎を習い、就職に向けて日々頑張っています。職業訓練校の詳しい情報は<a href="https://ten-navi.com/hacks/article-73-13725">こちら</a>
</div>
<h1>開発者紹介</h1>
<div>
<a href="ProfireOno.jsp" class="btn-square">小埜</a>
<a href="ProfireMotiduki.jsp" class="btn-square">望月</a>
<a href="ProfireKiyohara.jsp" class="btn-square">清原</a>
</div>
<h1>作品についての注意事項</h1>
<div class="box6">我々が作成した作品にはEclipsの設定をしないと閲覧できないことがあります。</div><p>
<div>
<div class="box6">まずサーバーをダブルクリックします。</div>
<img src="../picture/1.png">
<div class="box6">次に「起動構成を開く」をクリックします</div>
<img src="../picture/2.png">
<div class="box6">画面が開いたら、引数のタブを選択します。</div>
<img src="../picture/3.png">
<div class="box6">その他にチェックを入れて、フォルダ(srcやWebContentなどが入ってる)を選択します。</div>
<img src="../picture/4.png">
<div class="box6">最後にサーバーを再起動します。これで設定は完了です。</div>
</div>
</body>
</html>