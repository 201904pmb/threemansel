<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
h1{
text-align: center;
}
.center{
text-align: center;
}
</style>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/style2.css">
<link rel="stylesheet" type="text/css" href="../css/style3.css">
<link rel="stylesheet" type="text/css" href="../css/style4.css">
<link rel="stylesheet" type="text/css" href="../css/fontSize.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>自己紹介</h1>
<h3>名前</h3>
・小埜　耀永(オノ　アキヒサ)<p>
<h3>性別</h3>
・男
<h3>生年月日</h3>
・2000年2月4日
<h3>趣味</h3>
・アニメ鑑賞
・グッズ集め
<h3>自己PR</h3>
<div class="box6">
元々IT等に興味があり、今までは情報を見る側や公共のシステムを利用する側でしたが次第に作る側に興味を持ち始めました。

この出来事をきっかけに、IT企業について調べ、ハローワークの紹介を通してゲーム・アプリ・Webクリエイターの訓練校に半年間通っています。

養成所には5月から10月まで(就職が決まり次第途中退校可能)入学し、下記のことを学びました。
<p>
・Java<p>
・html/css<p>
・データベース<p>
・JSP/サーブレット<p>
・C♯<p>
・Unity<p>
・JavaScript<p>
・WordPress<p>

専門学生の卒業制作には敵いませんが、短期間で複数の作品を作ることに関しては他の学生達に引けを取らないと思います。
<p>
【主な習得能力】<p>
・Javaでオブジェクト指向を習得。<p>
・html/cssの基礎を習得。<p>
・データベースのSQLでCRUDを習得。<p>
・JSP/サーブレットで簡単なWebアプリケーションを作成。<p>
・C♯で複数のWindowアプリケーションを作成。<p>
・Unityにて短期間で複数のゲームと多彩なステージを作成。<p>
・JavaScriptでJqueryとAJAXによるデータ通信技術を習得。<p>
・Wordpressでインストールから簡単なカスタマイズ技術を習得。<p>
</div>
<h1>自分の作品</h1>

<h3>Unity</h3>
<h3>たま転がし</h3>
<p>
<div class="box2">
<a href="http://localhost:8080/threeManSel/gameCollection/index.html"><img src="../picture/game1.png"></a>
</div>

<div class="box6">
・キーボードの十字キーを使って玉を移動させ、各ステージにおいてあるアイテムをすべて集めるゲームを作成しました。
	玉を動かすコードや、格ステージへ移動させるコードなど苦労する点が多く、なかなか達成感のある作品でした。
</div>
<h3>C#</h3>
<h3>カラーリスト</h3>
<a href="\threeManSel\PlayServlet?param=work%5Cono%5Cc%23%5CColorList%5CColorList%5Cbin%5CDebug%5CColorList.exe">
<img src="../picture/collor.png">
</a>
<div class="box6">
Web等のデザインをするときのカラー番号を表示するアプリを作りました。
</div>
<h3>アイコンジェネレーター</h3>
<a href="\threeManSel\PlayServlet?param=work%5Cono%5Cc%23%5CIcon%5CIcon%5Cbin%5CDebug%5CIcon.exe">
<img src="../picture/icon.png">
</a>
<div class="box6">
ラインやTwitterなどの便利ツールのアイコンを作成するアプリです。
</div>
<a href="home.jsp">戻る</a>
</body>
</html>