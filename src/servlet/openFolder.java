package servlet;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class openFolder
 */
@WebServlet("/openFolder")
public class openFolder extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Desktop.getDesktop().open(new File(System.getProperty("user.dir") + "/work/kiyohara/c#/mnist/Mnist/bin/Debug"));
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String url = request.getHeader("Referer");
		out.println("<html>");
		out.println("<a href=" + url + ">戻る</a>");
		out.println("<h1>フォルダが開くので、その中のMnist.exeを起動してください</h1>");
		out.println("</html>");
	}
}
