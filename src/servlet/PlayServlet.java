package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PlayServlet
 */
@WebServlet("/PlayServlet")
public class PlayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String execPath = request.getParameter("param");
		System.out.println(System.getProperty("user.dir"));
		try {
			ProcessBuilder pb = new ProcessBuilder("cmd", "/c", execPath);
			pb.inheritIO();
			Process p = pb.start();
			p.waitFor();
		} catch(IOException ex) {
			ex.printStackTrace();
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String url = request.getHeader("Referer");
		out.println("<html>");
		out.println("<a href=" + url + ">戻る</a>");
		out.println("</html>");
	}
}
