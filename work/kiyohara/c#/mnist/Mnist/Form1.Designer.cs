﻿namespace Mnist
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.score0Label = new System.Windows.Forms.Label();
            this.score7Label = new System.Windows.Forms.Label();
            this.score6Label = new System.Windows.Forms.Label();
            this.score5Label = new System.Windows.Forms.Label();
            this.score4Label = new System.Windows.Forms.Label();
            this.score3Label = new System.Windows.Forms.Label();
            this.score2Label = new System.Windows.Forms.Label();
            this.score1Label = new System.Windows.Forms.Label();
            this.score9Label = new System.Windows.Forms.Label();
            this.score8Label = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Info;
            this.pictureBox1.Location = new System.Drawing.Point(490, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(280, 280);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDownEvent);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseDragged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(511, 380);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(259, 45);
            this.button1.TabIndex = 1;
            this.button1.Text = "消す";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.RemovePaint);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(511, 318);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(259, 44);
            this.button2.TabIndex = 2;
            this.button2.Text = "ネットワークに推論させる";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Predict);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox2.Location = new System.Drawing.Point(456, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(28, 28);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // score0Label
            // 
            this.score0Label.AutoSize = true;
            this.score0Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score0Label.Location = new System.Drawing.Point(122, 9);
            this.score0Label.Name = "score0Label";
            this.score0Label.Size = new System.Drawing.Size(90, 34);
            this.score0Label.TabIndex = 4;
            this.score0Label.Text = "?????";
            // 
            // score7Label
            // 
            this.score7Label.AutoSize = true;
            this.score7Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score7Label.Location = new System.Drawing.Point(122, 247);
            this.score7Label.Name = "score7Label";
            this.score7Label.Size = new System.Drawing.Size(90, 34);
            this.score7Label.TabIndex = 5;
            this.score7Label.Text = "?????";
            // 
            // score6Label
            // 
            this.score6Label.AutoSize = true;
            this.score6Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score6Label.Location = new System.Drawing.Point(122, 213);
            this.score6Label.Name = "score6Label";
            this.score6Label.Size = new System.Drawing.Size(90, 34);
            this.score6Label.TabIndex = 6;
            this.score6Label.Text = "?????";
            // 
            // score5Label
            // 
            this.score5Label.AutoSize = true;
            this.score5Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score5Label.Location = new System.Drawing.Point(122, 179);
            this.score5Label.Name = "score5Label";
            this.score5Label.Size = new System.Drawing.Size(90, 34);
            this.score5Label.TabIndex = 7;
            this.score5Label.Text = "?????";
            // 
            // score4Label
            // 
            this.score4Label.AutoSize = true;
            this.score4Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score4Label.Location = new System.Drawing.Point(122, 145);
            this.score4Label.Name = "score4Label";
            this.score4Label.Size = new System.Drawing.Size(90, 34);
            this.score4Label.TabIndex = 8;
            this.score4Label.Text = "?????";
            // 
            // score3Label
            // 
            this.score3Label.AutoSize = true;
            this.score3Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score3Label.Location = new System.Drawing.Point(122, 111);
            this.score3Label.Name = "score3Label";
            this.score3Label.Size = new System.Drawing.Size(90, 34);
            this.score3Label.TabIndex = 9;
            this.score3Label.Text = "?????";
            // 
            // score2Label
            // 
            this.score2Label.AutoSize = true;
            this.score2Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score2Label.Location = new System.Drawing.Point(122, 77);
            this.score2Label.Name = "score2Label";
            this.score2Label.Size = new System.Drawing.Size(90, 34);
            this.score2Label.TabIndex = 10;
            this.score2Label.Text = "?????";
            // 
            // score1Label
            // 
            this.score1Label.AutoSize = true;
            this.score1Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score1Label.Location = new System.Drawing.Point(122, 43);
            this.score1Label.Name = "score1Label";
            this.score1Label.Size = new System.Drawing.Size(90, 34);
            this.score1Label.TabIndex = 11;
            this.score1Label.Text = "?????";
            // 
            // score9Label
            // 
            this.score9Label.AutoSize = true;
            this.score9Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score9Label.Location = new System.Drawing.Point(122, 315);
            this.score9Label.Name = "score9Label";
            this.score9Label.Size = new System.Drawing.Size(90, 34);
            this.score9Label.TabIndex = 12;
            this.score9Label.Text = "?????";
            // 
            // score8Label
            // 
            this.score8Label.AutoSize = true;
            this.score8Label.Font = new System.Drawing.Font("MS UI Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.score8Label.Location = new System.Drawing.Point(122, 281);
            this.score8Label.Name = "score8Label";
            this.score8Label.Size = new System.Drawing.Size(90, 34);
            this.score8Label.TabIndex = 13;
            this.score8Label.Text = "?????";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Location = new System.Drawing.Point(358, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(126, 84);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ネットワークモデル選択";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 62);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(91, 16);
            this.radioButton3.TabIndex = 20;
            this.radioButton3.Text = "モデル3を使用";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.ChangeToModel3);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 18);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(91, 16);
            this.radioButton1.TabIndex = 18;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "モデル1を使用";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.ChangeToModel1);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 40);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(91, 16);
            this.radioButton2.TabIndex = 19;
            this.radioButton2.Text = "モデル2を使用";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.ChangeToModel2);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.score8Label);
            this.Controls.Add(this.score9Label);
            this.Controls.Add(this.score1Label);
            this.Controls.Add(this.score2Label);
            this.Controls.Add(this.score3Label);
            this.Controls.Add(this.score4Label);
            this.Controls.Add(this.score5Label);
            this.Controls.Add(this.score6Label);
            this.Controls.Add(this.score7Label);
            this.Controls.Add(this.score0Label);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label score0Label;
        private System.Windows.Forms.Label score7Label;
        private System.Windows.Forms.Label score6Label;
        private System.Windows.Forms.Label score5Label;
        private System.Windows.Forms.Label score4Label;
        private System.Windows.Forms.Label score3Label;
        private System.Windows.Forms.Label score2Label;
        private System.Windows.Forms.Label score1Label;
        private System.Windows.Forms.Label score9Label;
        private System.Windows.Forms.Label score8Label;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
    }
}

