﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TensorFlow;
using System.IO;

namespace Mnist
{
    class Model
    {
        TFGraph graph;
        TFSession session;
        public Model(string modelPath)
        {
            this.graph = new TFGraph();
            var modelBinary = File.ReadAllBytes(modelPath);
            graph.Import(modelBinary, "");
            this.session = new TFSession(this.graph);
        }

        public float[][] Predict(float[][][][] inputData)
        {
            var runner = session.GetRunner();
            TFTensor inputTensor = inputData;
            runner.AddInput(graph["input_folder"][0], inputTensor);
            runner.Fetch(graph["output"][0]);
            var result = runner.Run()[0];
            return (float[][])result.GetValue(true);
        }
    }
}
