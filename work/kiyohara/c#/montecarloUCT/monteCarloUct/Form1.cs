﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace monteCarloUct
{
    public partial class Form1 : Form
    {
        SlotMachines slotMachines;
        List<int> useSlotIndex;
        Random random;
        List<Label> constXLabels;
        List<Label> avgCoinNumLabels;
        List<Label> eachSimuNumLabels;
        List<Label> ucbValueLabels;
        List<CheckBox> useSlotCheckBoxes;


        public Form1()
        {
            InitializeComponent();

            var slotCoinConfigs = new List<List<int>>();
            slotCoinConfigs.Add(new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
            slotCoinConfigs.Add(new List<int> { 1, 3, 5, 7, 9 });
            slotCoinConfigs.Add(new List<int> { 2, 4, 6, 8, 10 });
            slotCoinConfigs.Add(new List<int>());
            for (int i = 0; i < 99; i++)
            {
                slotCoinConfigs[3].Add(0);
            }
            slotCoinConfigs[3].Add(1000);

            this.slotMachines = new SlotMachines(slotCoinConfigs);
            this.random = new Random();
            this.constXLabels = new List<Label>
            {
                this.slot1ConstXLabel,
                this.slot2ConstXLabel,
                this.slot3ConstXLabel,
                this.slot4ConstXLabel,
            };

            this.useSlotCheckBoxes = new List<CheckBox>
            {
                this.useSlot1CheckBox,
                this.useSlot2CheckBox,
                this.useSlot3CheckBox,
                this.useSlot4CheckBox,
            };

            this.avgCoinNumLabels = new List<Label> {
                this.slot1AvgCoinNumLabel,
                this.slot2AvgCoinNumLabel,
                this.slot3AvgCoinNumLabel,
                this.slot4AvgCoinNumLabel,
            };

            this.eachSimuNumLabels = new List<Label> {
                this.slot1SimuNumLabel,
                this.slot2SimuNumLabel,
                this.slot3SimuNumLabel,
                this.slot4SimuNumLabel,
            };

            this.ucbValueLabels = new List<Label>
            {
                this.slot1UcbLabel,
                this.slot2UcbLabel,
                this.slot3UcbLabel,
                this.slot4UcbLabel,
            };
        }

        private void UpdateUcbParameterLabels()
        {
            double X = double.Parse(this.constXTextBox.Text);
            foreach (int index in this.useSlotIndex)
            {
                this.avgCoinNumLabels[index].Text = this.slotMachines.GetMachines()[index].GetAverageValue().ToString("0.0000");
                this.eachSimuNumLabels[index].Text = this.slotMachines.GetMachines()[index].GetEachSimuNum().ToString();
                this.ucbValueLabels[index].Text = this.slotMachines.GetMachines()[index].GetUCB(X).ToString("0.0000");
            }
            this.totalSimuNumLabel.Text = this.slotMachines.GetMachines()[this.useSlotIndex[0]].GetTotalSimuNum().ToString();
        }

        private void StartPlay(object sender, EventArgs e)
        {

            constXTextBox.Enabled = false;
            startButton.Enabled = false;
            ucbPolicySimuNumTextBox.Enabled = true;
            ucbPolicyPlayButton.Enabled = true;

            foreach (CheckBox checkBox in this.useSlotCheckBoxes)
            {
                checkBox.Enabled = false;
            }

            bool[] useSlotCheckBoxCheckedList = {
                useSlot1CheckBox.Checked,
                useSlot2CheckBox.Checked,
                useSlot3CheckBox.Checked,
                useSlot4CheckBox.Checked,
            };

            var useSlotIndex = new List<int>();
            for (int i = 0; i < useSlotCheckBoxCheckedList.Length; i++)
            {
                if (useSlotCheckBoxCheckedList[i])
                {
                    useSlotIndex.Add(i);
                }
            }
            this.useSlotIndex = useSlotIndex;

            foreach (int index in this.useSlotIndex)
            {
                constXLabels[index].Text = constXTextBox.Text;
            }

            slotMachines.FirstPlay(this.useSlotIndex, this.random);
            UpdateUcbParameterLabels();
        }

        private void UcbPolicyPlayEvent(object sender, EventArgs e)
        {
            int simuNum = int.Parse(ucbPolicySimuNumTextBox.Text);
            double X = (double)int.Parse(constXTextBox.Text);

            for (int i = 0; i < simuNum; i++)
            {
                slotMachines.UCBPolicyPlay(useSlotIndex, X, this.random);
            }
            UpdateUcbParameterLabels();

        }

        private void SelectedResetButton(object sender, EventArgs e)
        {
            constXTextBox.Enabled = true;
            startButton.Enabled = true;
            ucbPolicySimuNumTextBox.Enabled = false;
            ucbPolicyPlayButton.Enabled = false;

            foreach (CheckBox checkBox in this.useSlotCheckBoxes)
            {
                checkBox.Enabled = true;
            }

            foreach (int index in useSlotIndex)
            {
                constXLabels[index].Text = "未確定";
                avgCoinNumLabels[index].Text = "0";
                eachSimuNumLabels[index].Text = "0";
                ucbValueLabels[index].Text = "0.0";
            }
            this.slotMachines.ResetUcbParameter();
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
