﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace monteCarloUct
{
    class Model
    {
        public static double UCB(double averageValue, int totalSimuNum, int eachSimuNum, float X)
        {
            return averageValue + (X * Math.Sqrt(2 * Math.Log(totalSimuNum) / eachSimuNum));
        }
    }

    class SlotMachine
    {
        private double totalValue;
        private int totalSimuNum;
        private int eachSimuNum;
        private List<int> coinConfig;

        public SlotMachine(List<int> coinConfig)
        {
            this.totalValue = 0.0;
            this.totalSimuNum = 0;
            this.eachSimuNum = 0;
            this.coinConfig = coinConfig;
        }

        public double GetUCB(double X) {
            var doubleTotalSimuNum = (double)this.totalSimuNum;
            var doubleEachSimuNum = (double)this.eachSimuNum;
            return this.GetAverageValue() + (X * Math.Sqrt(2 * Math.Log(doubleTotalSimuNum) / doubleEachSimuNum));
        }

        public void FirstPlay(int machineNum, Random random)
        {
            this.totalSimuNum += machineNum;
            this.eachSimuNum += 1;
            int index = random.Next(this.coinConfig.Count);
            this.totalValue += this.coinConfig[index];
        }

        public void Play(Random random)
        {
            this.totalSimuNum += 1;
            this.eachSimuNum += 1;
            int index = random.Next(this.coinConfig.Count);
            this.totalValue += this.coinConfig[index];
        }
        
        public void TotalSimuNumInc()
        {
            this.totalSimuNum += 1;
        }

        public int GetTotalSimuNum()
        {
            return this.totalSimuNum;
        }

        public int GetEachSimuNum()
        {
            return this.eachSimuNum;
        }

        public double GetAverageValue()
        {
            return this.totalValue / this.eachSimuNum;
        }

        public void ResetUcbParameter()
        {
            this.totalValue = 0.0;
            this.totalSimuNum = 0;
            this.eachSimuNum = 0;
        }
    }

    class SlotMachines
    {
        List<SlotMachine> machines;

        public SlotMachines(List<List<int>> coinConfigs)
        {
            this.machines = new List<SlotMachine>();
            foreach (List<int> coinConfig in coinConfigs)
            {
                this.machines.Add(new SlotMachine(coinConfig));
            }
        }

        public void FirstPlay(List<int> useIndices, Random random)
        {
            foreach (int index in useIndices)
            {
                this.machines[index].FirstPlay(useIndices.Count, random);
            }
        }

        public int GetMaxUcbIndex(List<int> useIndices, double X)
        { 
            double maxUcb = this.machines[useIndices[0]].GetUCB(X);
            int maxUcbIndex = useIndices[0];

            foreach (int index in useIndices)
            {
                double ucb = this.machines[index].GetUCB(X);
                if (ucb > maxUcb)
                {
                    maxUcb = ucb;
                    maxUcbIndex = index;
                }
            }
            return maxUcbIndex;
        }

        public void UCBPolicyPlay(List<int> useIndices, double X, Random random)
        {
            int maxUcbMachineIndex = GetMaxUcbIndex(useIndices, X);
            this.machines[maxUcbMachineIndex].Play(random);
            foreach (int index in useIndices)
            {
                if (index == maxUcbMachineIndex)
                {
                    continue;
                }
                this.machines[index].TotalSimuNumInc();
            }
        }

        public List<SlotMachine> GetMachines()
        {
            return this.machines;
        }

        public void ResetUcbParameter()
        {
            foreach (SlotMachine machine in this.machines)
            {
                machine.ResetUcbParameter();
            }
        }
    }
}
