﻿namespace FlashANZAN
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_speedGroup = new System.Windows.Forms.GroupBox();
            this.rb_superLowSpeed = new System.Windows.Forms.RadioButton();
            this.rb_lowSpeed = new System.Windows.Forms.RadioButton();
            this.rb_middleSpeed = new System.Windows.Forms.RadioButton();
            this.rb_highSpeed = new System.Windows.Forms.RadioButton();
            this.rb_superHighSpeed = new System.Windows.Forms.RadioButton();
            this.lbl_numDisp = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.gb_difficulty = new System.Windows.Forms.GroupBox();
            this.rb_oneDigit = new System.Windows.Forms.RadioButton();
            this.rb_toTwoDigits = new System.Windows.Forms.RadioButton();
            this.rb_toThreeDigits = new System.Windows.Forms.RadioButton();
            this.cb_calcTimes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_answerCheck = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_answer = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Timers.Timer();
            this.gb_speedGroup.SuspendLayout();
            this.gb_difficulty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_speedGroup
            // 
            this.gb_speedGroup.Controls.Add(this.rb_superLowSpeed);
            this.gb_speedGroup.Controls.Add(this.rb_lowSpeed);
            this.gb_speedGroup.Controls.Add(this.rb_middleSpeed);
            this.gb_speedGroup.Controls.Add(this.rb_highSpeed);
            this.gb_speedGroup.Controls.Add(this.rb_superHighSpeed);
            this.gb_speedGroup.Location = new System.Drawing.Point(30, 34);
            this.gb_speedGroup.Name = "gb_speedGroup";
            this.gb_speedGroup.Size = new System.Drawing.Size(107, 214);
            this.gb_speedGroup.TabIndex = 0;
            this.gb_speedGroup.TabStop = false;
            this.gb_speedGroup.Text = "速度";
            // 
            // rb_superLowSpeed
            // 
            this.rb_superLowSpeed.AutoSize = true;
            this.rb_superLowSpeed.Location = new System.Drawing.Point(20, 170);
            this.rb_superLowSpeed.Name = "rb_superLowSpeed";
            this.rb_superLowSpeed.Size = new System.Drawing.Size(59, 16);
            this.rb_superLowSpeed.TabIndex = 4;
            this.rb_superLowSpeed.TabStop = true;
            this.rb_superLowSpeed.Text = "超低速";
            this.rb_superLowSpeed.UseVisualStyleBackColor = true;
            // 
            // rb_lowSpeed
            // 
            this.rb_lowSpeed.AutoSize = true;
            this.rb_lowSpeed.Location = new System.Drawing.Point(20, 135);
            this.rb_lowSpeed.Name = "rb_lowSpeed";
            this.rb_lowSpeed.Size = new System.Drawing.Size(47, 16);
            this.rb_lowSpeed.TabIndex = 3;
            this.rb_lowSpeed.TabStop = true;
            this.rb_lowSpeed.Text = "低速";
            this.rb_lowSpeed.UseVisualStyleBackColor = true;
            // 
            // rb_middleSpeed
            // 
            this.rb_middleSpeed.AutoSize = true;
            this.rb_middleSpeed.Checked = true;
            this.rb_middleSpeed.Location = new System.Drawing.Point(20, 102);
            this.rb_middleSpeed.Name = "rb_middleSpeed";
            this.rb_middleSpeed.Size = new System.Drawing.Size(47, 16);
            this.rb_middleSpeed.TabIndex = 2;
            this.rb_middleSpeed.TabStop = true;
            this.rb_middleSpeed.Text = "普通";
            this.rb_middleSpeed.UseVisualStyleBackColor = true;
            // 
            // rb_highSpeed
            // 
            this.rb_highSpeed.AutoSize = true;
            this.rb_highSpeed.Location = new System.Drawing.Point(20, 67);
            this.rb_highSpeed.Name = "rb_highSpeed";
            this.rb_highSpeed.Size = new System.Drawing.Size(47, 16);
            this.rb_highSpeed.TabIndex = 1;
            this.rb_highSpeed.TabStop = true;
            this.rb_highSpeed.Text = "高速";
            this.rb_highSpeed.UseVisualStyleBackColor = true;
            // 
            // rb_superHighSpeed
            // 
            this.rb_superHighSpeed.AutoSize = true;
            this.rb_superHighSpeed.Location = new System.Drawing.Point(20, 32);
            this.rb_superHighSpeed.Name = "rb_superHighSpeed";
            this.rb_superHighSpeed.Size = new System.Drawing.Size(59, 16);
            this.rb_superHighSpeed.TabIndex = 0;
            this.rb_superHighSpeed.TabStop = true;
            this.rb_superHighSpeed.Text = "超高速";
            this.rb_superHighSpeed.UseVisualStyleBackColor = true;
            // 
            // lbl_numDisp
            // 
            this.lbl_numDisp.Font = new System.Drawing.Font("MS UI Gothic", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_numDisp.Location = new System.Drawing.Point(408, 49);
            this.lbl_numDisp.Name = "lbl_numDisp";
            this.lbl_numDisp.Size = new System.Drawing.Size(310, 180);
            this.lbl_numDisp.TabIndex = 1;
            this.lbl_numDisp.Text = "???";
            this.lbl_numDisp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(290, 101);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 2;
            this.btn_start.Text = "スタート";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // gb_difficulty
            // 
            this.gb_difficulty.Controls.Add(this.rb_oneDigit);
            this.gb_difficulty.Controls.Add(this.rb_toTwoDigits);
            this.gb_difficulty.Controls.Add(this.rb_toThreeDigits);
            this.gb_difficulty.Location = new System.Drawing.Point(155, 34);
            this.gb_difficulty.Name = "gb_difficulty";
            this.gb_difficulty.Size = new System.Drawing.Size(97, 137);
            this.gb_difficulty.TabIndex = 3;
            this.gb_difficulty.TabStop = false;
            this.gb_difficulty.Text = "難易度";
            // 
            // rb_oneDigit
            // 
            this.rb_oneDigit.AutoSize = true;
            this.rb_oneDigit.Location = new System.Drawing.Point(17, 102);
            this.rb_oneDigit.Name = "rb_oneDigit";
            this.rb_oneDigit.Size = new System.Drawing.Size(41, 16);
            this.rb_oneDigit.TabIndex = 2;
            this.rb_oneDigit.TabStop = true;
            this.rb_oneDigit.Text = "1桁";
            this.rb_oneDigit.UseVisualStyleBackColor = true;
            // 
            // rb_toTwoDigits
            // 
            this.rb_toTwoDigits.AutoSize = true;
            this.rb_toTwoDigits.Checked = true;
            this.rb_toTwoDigits.Location = new System.Drawing.Point(17, 67);
            this.rb_toTwoDigits.Name = "rb_toTwoDigits";
            this.rb_toTwoDigits.Size = new System.Drawing.Size(60, 16);
            this.rb_toTwoDigits.TabIndex = 1;
            this.rb_toTwoDigits.TabStop = true;
            this.rb_toTwoDigits.Text = "2桁まで";
            this.rb_toTwoDigits.UseVisualStyleBackColor = true;
            // 
            // rb_toThreeDigits
            // 
            this.rb_toThreeDigits.AutoSize = true;
            this.rb_toThreeDigits.Location = new System.Drawing.Point(17, 32);
            this.rb_toThreeDigits.Name = "rb_toThreeDigits";
            this.rb_toThreeDigits.Size = new System.Drawing.Size(60, 16);
            this.rb_toThreeDigits.TabIndex = 0;
            this.rb_toThreeDigits.Text = "3桁まで";
            this.rb_toThreeDigits.UseVisualStyleBackColor = true;
            // 
            // cb_calcTimes
            // 
            this.cb_calcTimes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_calcTimes.FormattingEnabled = true;
            this.cb_calcTimes.Items.AddRange(new object[] {
            "3数の和",
            "4数の和",
            "5数の和",
            "6数の和",
            "7数の和",
            "8数の和",
            "9数の和",
            "10数の和"});
            this.cb_calcTimes.Location = new System.Drawing.Point(279, 58);
            this.cb_calcTimes.Name = "cb_calcTimes";
            this.cb_calcTimes.Size = new System.Drawing.Size(97, 20);
            this.cb_calcTimes.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(277, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "計算回数";
            // 
            // btn_answerCheck
            // 
            this.btn_answerCheck.Location = new System.Drawing.Point(290, 136);
            this.btn_answerCheck.Name = "btn_answerCheck";
            this.btn_answerCheck.Size = new System.Drawing.Size(75, 23);
            this.btn_answerCheck.TabIndex = 6;
            this.btn_answerCheck.Text = "答え合わせ";
            this.btn_answerCheck.UseVisualStyleBackColor = true;
            this.btn_answerCheck.Click += new System.EventHandler(this.AnswerCheckButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(200, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "解答を整数で入力して下さい";
            // 
            // tb_answer
            // 
            this.tb_answer.Location = new System.Drawing.Point(200, 220);
            this.tb_answer.Name = "tb_answer";
            this.tb_answer.Size = new System.Drawing.Size(144, 19);
            this.tb_answer.TabIndex = 8;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 286);
            this.Controls.Add(this.tb_answer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_answerCheck);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_calcTimes);
            this.Controls.Add(this.gb_difficulty);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.lbl_numDisp);
            this.Controls.Add(this.gb_speedGroup);
            this.Name = "Form1";
            this.Text = "フラッシュ暗算";
            this.gb_speedGroup.ResumeLayout(false);
            this.gb_speedGroup.PerformLayout();
            this.gb_difficulty.ResumeLayout(false);
            this.gb_difficulty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_speedGroup;
        private System.Windows.Forms.RadioButton rb_superLowSpeed;
        private System.Windows.Forms.RadioButton rb_lowSpeed;
        private System.Windows.Forms.RadioButton rb_middleSpeed;
        private System.Windows.Forms.RadioButton rb_highSpeed;
        private System.Windows.Forms.RadioButton rb_superHighSpeed;
        private System.Windows.Forms.Label lbl_numDisp;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.GroupBox gb_difficulty;
        private System.Windows.Forms.RadioButton rb_oneDigit;
        private System.Windows.Forms.RadioButton rb_toTwoDigits;
        private System.Windows.Forms.RadioButton rb_toThreeDigits;
        private System.Windows.Forms.ComboBox cb_calcTimes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_answerCheck;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_answer;
        private System.Timers.Timer timer1;
    }
}

